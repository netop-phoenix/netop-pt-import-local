<?php

class importLocal{

	public function check_empty($data){

            if(empty($data) || $data == "" || is_null($data)){

                $data = NULL;
            }

            return $data;
	}

	public function getStudentPersonID($data){
					
					$sql = "SELECT PersonID FROM ComMPerson WHERE PersonCode = ".$data." AND PersonTypeID = 0";
				    $stmt = $dbh->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$PersonID = $result['PersonID'];
				    }

	                return $this->check_empty($PersonID);
	}

	public function getSubjectID($data){

					$sql = "SELECT SubjectID FROM RegMSubject WHERE SubjectCode = ".$data." AND ReportSubjectCode = '".$data."'";
				    $stmt = $dbh->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$SubjectID = $result['SubjectID'];
				    }

	                return $this->check_empty($SubjectID);
	}

	public function getTermID($data){

				if($data == '_'){

					$TermID == 0;

				}else{

	                $sql = "SELECT TermID FROM RegBTerm WHERE TermSName = '%".$data."%'";
				    $stmt = $dbh->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$TermID = $result['TermID'];
				    }
				}
	                return $TermID;

	}

	public function array_mode($array, $justMode = 0){

                            $count = array();

                            foreach ( $array as $item) {

                                if ( isset($count[$item]) ) {
                                    $count[$item]++;
                                }else{
                                    $count[$item] = 1;
                                };

                            };

                            $mostcommon = '';
                            $iter = 0;

                            foreach ( $count as $k => $v ) {

                                if ( $v > $iter ) {
                                    $mostcommon = $k;
                                    $iter = $v;
                                };

                            };

                            if ($justMode == 0) {
                                return $mostcommon;
                            } else {
                                return array("mode" => $mostcommon, "count" => $iter);
                            }

	}

}