<?php
	// require('config/connect.php');
	// require('config/function.php');
	// require('config/importLocal.php');
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<title><?php include('config/title.html'); ?></title>
	
<!--[if lt IE 9]>
<script src="js/html5.js" type="text/javascript"></script>
<![endif]-->

<link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/shortcodes.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/animate.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/jplayer.blue.monday.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/flexslider.css' type='text/css' media='all' />

<!-- FontAwesome CSS -->
<link rel="stylesheet" type="text/css" media="all" href="fontawesome/css/font-awesome.css" />

<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.html'></script>
<script type='text/javascript' src='js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='js/hoverIntent.js'></script>
<script type='text/javascript' src='js/superfish.js'></script>
<script type='text/javascript' src='js/jquery.jplayer.min.js'></script>
<script type='text/javascript' src='js/jquery.preloadify.min.js'></script>
<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='js/jquery.fitvids.js'></script>
<script type='text/javascript' src='js/jquery.flexslider.js'></script>
<script type='text/javascript' src='js/waypoints.js'></script>
<script type='text/javascript' src='js/sys_custom.js'></script>
 
<!-- Google Fonts -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function GetInputValue () {
            var input = document.getElementById ("fileToUpload");
            alert (input.value);
        }
    </script>

    <?php
	    $config_file = fopen("config/config.txt","r");
		$i = 0;
		while(! feof($config_file))
			{
				
				$row = fgets($config_file);
				$row_db = ereg_replace('[[:space:]]+', '', trim($row));
				$db_con[$i] = $row_db;

				$i++;
			}
		fclose($config_file);
	?>

</head>

<body>
<!-- LayoutWrap -->
<div id="stretched" class="leftsidebar">
	<!-- bodyoverlay -->
	<div class="bodyoverlay"></div>
	<!-- Stickybar -->
	<div id="trigger" class="tarrow"></div>
	<div id="sticky">Enter the content which will be displayed in sticky bar </div>

	<!-- #wrapper -->
	<div id="wrapper">
<?php include('config/header.php');
//include('config/subheader.php'); ?>
	
		<div class="pagemid">
			<div class="inner">
				<div id="main">
					<div class="entry-content">

						<div class="post" style="min-height: 475px;">
							
							<div class="two_third">
								<h3 class="fancy-title textleft"  style="line-height:normal;">ตั้งค่าการเชื่อมต่อฐานข้อมูล</h3>
								<div class="wpcf7" id="wpcf7-f455-p28-o1">
								<form action="?Action=Update" method="post" class="wpcf7-form" novalidate="novalidate">
								
								<p>Mysql UserName
								<input type="text" name="mysql_user" value="<?php echo $db_con[0]; ?>" class="wpcf7-form-control" /></p>

								<p>Mysql Password
								<input type="password" name="mysql_pass" value="<?php echo $db_con[1]; ?>" class="wpcf7-form-control" /></p>

								<p>Mysql DataBase
								<input type="text" name="mysql_db" value="<?php echo $db_con[2]; ?>" class="wpcf7-form-control" /></p>

								<p>DataBase LocalSchool
								<input type="text" name="mdb_db" id="mdb_db" value="<?php echo $db_con[3]; ?>" class="wpcf7-form-control" /></p>

								<p style="margin-top:120px;"><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" /></p>
								<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
							</div><!-- .two_third-->
							
							<div class="one_third last">
							</div><!-- .one_third-->
							<div class="clear"></div>
						</div><!-- .POST -->
						<div class="clear"></div>

						<?php

						if(isset($_REQUEST['Action']) && $_REQUEST['Action'] == 'Update'){

							// echo 'user = '.$_POST['user'].'<br />';
							// echo 'pass = '.$_POST['pass'].'<br />';
							// echo 'db   = '.$_POST['db'].'<br />';

							//$db_con = array('mysql_user' => $_POST['mysql_user'], 'mysql_pass' => $_POST['mysql_pass'], 'mysql_db' => $_POST['mysql_db'], 'mdb_db' => $_POST['mdb_db']);

							// echo 'mysql_user = '.$db_con['mysql_user'].'<br />';
							// echo 'mysql_pass = '.$db_con['mysql_pass'].'<br />';
							// echo 'mysql_db = '.$db_con['mysql_db'].'<br />';
							// echo 'mdb_db = '.$db_con['mdb_db'].'<br />';

							// $file = 'config/config.txt';

							// $write = $db_con['mysql_user'] . "\n";
							// $write .= $db_con['mysql_pass'] . "\n";
							// $write .= $db_con['mysql_db'] . "\n";
							// $write .= $db_con['mdb_db'] . "\n";
							// fwrite($file, $write);

							$file = fopen('config/config.txt', 'w');
							$write = $_POST['mysql_user'] . PHP_EOL;
							$write .= $_POST['mysql_pass'] .PHP_EOL;
							$write .= $_POST['mysql_db'] . PHP_EOL;
							$write .= $_POST['mdb_db'] . PHP_EOL;
							fwrite($file, $write);

							fclose($file);

							echo "<meta http-equiv='refresh' content='0;url=config.php'>";





						}

						?>
						
					</div><!-- .entry-content -->
				</div><!-- #main -->
				<div class="clear"></div>
<?php //include('config/sidebar.php'); ?>
				
			</div><!-- .inner -->
		</div><!-- .pagemid -->
		
<?php include('config/footer.php'); ?>
	</div><!-- #wrapper -->
</div>
<?php include('config/back-top.html'); ?>
</body>
</html>