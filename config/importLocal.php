<?php

class importLocal{

	var $db;

	public function __construct()
    {
        global $dbh;
        $this->db = $dbh;
    }

	public function utf8_to_tis620($string) {
	   $str = $string;
	   $res = "";
	   for ($i = 0; $i < strlen($str); $i++) {
	      if (ord($str[$i]) == 224) {
	        $unicode = ord($str[$i+2]) & 0x3F;
	        $unicode |= (ord($str[$i+1]) & 0x3F) << 6;
	        $unicode |= (ord($str[$i]) & 0x0F) << 12;
	        $res .= chr($unicode-0x0E00+0xA0);
	        $i += 2;
	      } else {
	        $res .= $str[$i];
	      }
	   }
	   return $res;
	}

	// function tis620_to_utf8($in) {
	//    $out = "";
	//    for ($i = 0; $i < strlen($in); $i++)
	//    {
	//      if (ord($in[$i])
	//        $out .= $in[$i];
	//      else
	//        $out .= "&#" . (ord($in[$i]) - 161 + 3585) . ";";
	//    }
	//    return $out;
	// }

	public function tis620_to_utf8($text) {
	  $utf8 = "";
	  for ($i = 0; $i < strlen($text); $i++) {
	    $a = substr($text, $i, 1);
	    $val = ord($a);
	 
	    if ($val < 0x80) {
	      $utf8 .= $a;
	    } elseif ((0xA1 <= $val && $val < 0xDA) || (0xDF <= $val && $val <= 0xFB)) {
	      $unicode = 0x0E00+$val-0xA0;
	      $utf8 .= chr(0xE0 | ($unicode >> 12));
	      $utf8 .= chr(0x80 | (($unicode >> 6) & 0x3F));
	      $utf8 .= chr(0x80 | ($unicode & 0x3F));
	    }
	  }
	  return $utf8;
	}


	public function teacherCode_CheckNull($TeacherCode){

	  if($TeacherCode == '' || $TeacherCode == ' ' || is_null($TeacherCode)){
	      
	      $TeacherCode = 0;
	    }

	    return $TeacherCode;
	}


	public function IDYearData_Split($data){

			$text = $data;
			$CurriculumNo = $text[0];
			$AcademicYear = substr($text, 1, 4);					
			$TermSName = $text[5];										
			$Class = $text[6].$text[7].$text[8];
			$ClassSName =  $this->tis620_to_utf8($Class);
			$RoomName = substr($text, 9, 10);


	  		$array = array('CurriculumNo'=>$CurriculumNo,'AcademicYear'=>$AcademicYear, 'TermSName'=>$TermSName, 'ClassSName'=>$ClassSName, 'RoomName'=>$RoomName);
	  		return $array;

	}


	public function check_empty($data)
	{

            if(empty($data) || $data == "" || is_null($data)){

                $data = NULL;
            }

            return $data;
	}

	public function getStudentPersonID($data)
	{
					$PersonID = 0;
					$sql = "SELECT PersonID FROM ComMPerson WHERE PersonCode = ".$data." AND PersonTypeID = 0";
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$PersonID = $result['PersonID'];
				    }

	                return $this->check_empty($PersonID);
	}

	public function getTeacherPersonID($data)
	{
					
					$sql = "SELECT PersonID FROM ComMPerson WHERE PersonCode = ".$data." AND PersonTypeID = 1";
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$data = $result['PersonID'];
				    }

	                return $this->check_empty($data);
	}

	public function getCurriculumID($data){

			$sql = "SELECT CurriculumID FROM RegBCurriculum WHERE CurriculumNo = ".$data;
		    $stmt = $this->db->prepare($sql);
		    $stmt->execute();
		    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

		    	$data = $result['CurriculumID'];
		    }

		    return $this->check_empty($data);
	}

	public function getSubjectID($SubjectCode,$AcademicYear)
	{
					$data = NULL;
					$sql = "SELECT SubjectID FROM RegMSubject WHERE SubjectCode = '".$SubjectCode."' AND AcademicYear = ".$AcademicYear;
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$data = $result['SubjectID'];
				    }

	                return $this->check_empty($data);
	}

	public function getSubjectTypeID($data){

			$sql = "SELECT SubjectTypeID FROM RegBSubjectType WHERE SubjectTypeName LIKE '".$data."'";
		    $stmt = $this->db->prepare($sql);
		    $stmt->execute();
		    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

		    	$data = $result['SubjectTypeID'];
		    }

		    return $this->check_empty($data);
	}

	public function getTermID($data)
	{

				if($this->tis620_to_utf8($data) == '_'){

					$data == 0;

				}else{

	                $sql = "SELECT TermID FROM RegBTerm WHERE TermSName = '%".$data."%'";
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$data = $result['TermID'];
				    }
				}
	                return $data;

	}

	public function getClassID($data)
	{

					$sql = "SELECT ClassID FROM RegBClass WHERE ClassSName = '".$data."'";
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$data = $result['ClassID'];
				    }

	                return $this->check_empty($data);
	}

	public function getRoomID($data)
	{

					$sql = "SELECT RoomID FROM RegBClass WHERE ClassSName = '".$data."'";
				    $stmt = $this->db->prepare($sql);
				    $stmt->execute();
				    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

				    	$data = $result['RoomID'];
				    }

	                return $this->check_empty($data);
	}

	public function array_mode($array, $justMode = 0)
	{

                            $count = array();

                            foreach ( $array as $item) {

                                if ( isset($count[$item]) ) {
                                    $count[$item]++;
                                }else{
                                    $count[$item] = 1;
                                };

                            };

                            $mostcommon = '';
                            $iter = 0;

                            foreach ( $count as $k => $v ) {

                                if ( $v > $iter ) {
                                    $mostcommon = $k;
                                    $iter = $v;
                                };

                            };

                            if ($justMode == 0) {
                                return $mostcommon;
                            } else {
                                return array("mode" => $mostcommon, "count" => $iter);
                            }

	}


	public function getAssessID($data)
	{
		if(is_null($data) || $data == '' || empty($data)){

			return 0;
		}else{

	        $sql = "SELECT AssessID FROM RegBAssess WHERE AssessScore = '".$data."'";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

					$data = $result['AssessID'];
				}

	        return $this->check_empty($data);
    	}
	}


	public function getGradeID($data)
	{
		if(is_null($data) || $data == '' || empty($data)){

			return 0;
		}else{

			$sql = "SELECT GradeID FROM RegBGrade WHERE GradeName = '".$data."'";
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
			while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

					$data = $result['GradeID'];
				}
		    return $this->check_empty($data);

		}
	}


	public function getGradeIDFromScore($P100Score)
	{

		$sql = "SELECT * FROM RegBScoreGrade LIMIT 1";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		while($result = $stmt->fetch(PDO::FETCH_ASSOC)){

				switch ($P100Score) {
					case ($P100Score < $result['Begin1']) :
						    	$Grade = '0';
						break;

					case ($P100Score >= $result['Begin1']) && ($P100Score < $result['Begin1Haft']) :
						    	$Grade = '1';
						break;

					case ($P100Score >= $result['Begin1Haft']) && ($P100Score < $result['Begin2']) :
						    	$Grade = '1.5';
						break;

					case ($P100Score >= $result['Begin2']) && ($P100Score < $result['Begin2Haft']) :
						    	$Grade = '2';
						break;

					case ($P100Score >= $result['Begin2Haft']) && ($P100Score < $result['Begin3']) :
						    	$Grade = '2.5';
						break;

					case ($P100Score >= $result['Begin3']) && ($P100Score < $result['Begin3Haft']) :
						    	$Grade = '3';
						break;

					case ($P100Score >= $result['Begin3Haft']) && ($P100Score < $result['Begin4']) :
						    	$Grade = '3.5';
						break;

					case ($P100Score >= $result['Begin4']) :
						    	$Grade = '4';
						break;
						    		
					default:
						    $Grade = '-';
						break;
				}

		}
			return $this->getGradeID($Grade);

	}


}