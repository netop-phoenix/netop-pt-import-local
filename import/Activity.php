<?php

require_once('config/connect.php');
require_once('config/function.php');
require_once('config/importLocal.php');

	if($_POST['ImportType'] == 'Activity'){
		$i = 1;

		$strSQL = "SELECT * FROM TBStudentActivity  WHERE AYear = ".$_POST['AcademicYear'];
		$objExec = odbc_exec($objConnect, $strSQL) or die ("Error Execute : [".$strSQL."]");

		while($objResult = odbc_fetch_array($objExec))
		{
			echo $objResult['IDStudent'].' - '.$importLocal->tis620_to_utf8($objResult['ActivityCode']).' - '.$objResult['AYear'].' - '.$objResult['Term'].' - '.$importLocal->tis620_to_utf8($objResult['ActEval']).'<br />';
			
			$GradeName = $importLocal->tis620_to_utf8($objResult['ActEval']);
			$SubjectCode = $importLocal->tis620_to_utf8($objResult["ActivityCode"]);
		    $PersonID = $importLocal->getStudentPersonID($importLocal->tis620_to_utf8($objResult["IDStudent"]));
		    $SubjectID = $importLocal->getSubjectID($importLocal->tis620_to_utf8($objResult["ActivityCode"]), $_POST['AcademicYear']);
		    $TermID = $importLocal->getTermID($objResult['Term']);
		    $GradeID = $importLocal->getGradeID($GradeName);
		    $ClassID = 0;
		    $RoomID = 0;
		    $TeacherID = 0;

		    echo 'IDStudent = '.$importLocal->tis620_to_utf8($objResult["IDStudent"]).' : PersonID = '.$PersonID.'<br />';
		 //    if(is_null($GradeID) || $GradeID == '' || empty($GradeID)){

			// 	$GradeID = 0;
			// }

			//echo '['.$i.'] : PersonID = '.$PersonID.' : AcademicYear = '.$objResult['AYear'].' : SubjectID = '.$SubjectID.' : TermID = '.$TermID.' : GradeID = '.$GradeID.'<br />';

			$sqlCHKRegis = "SELECT ClassID, RoomID FROM RegTTeach WHERE SubjectID = :SubjectID AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
			$queryCHKRegis = $dbh->prepare($sqlCHKRegis);
			$queryCHKRegis->execute(array(':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST['AcademicYear'], ':TermID'=>$TermID));
			$CHKRegis = $queryCHKRegis->rowCount();

			if ($CHKRegis == 0) {
				echo 'Subject Register'.'<br />';

				$sqlCR = "SELECT ClassID, RoomID FROM RegTClassRoom WHERE PersonID = :PersonID AND AcademicYear = :AcademicYear";
				$queryCR = $dbh->prepare($sqlCR);
				$queryCR->execute(array(':PersonID'=>$PersonID, ':AcademicYear'=>$_POST['AcademicYear']));
				$chkCR = $queryCR->rowCount();
				while ($resultCR = $queryCR->fetch(PDO::FETCH_ASSOC)) {

					echo 'Select ClassRoom <br />';

						$ClassID = $resultCR['ClassID'];
						$RoomID = $resultCR['RoomID'];					
				}


				if ($chkCR == 0) {

					$stdYearNow = 0;
					$stdClassIDNow = 0;
					$stdRoomIDNow = 0;
					$yearDiff = 0;

					$sqlCRNF = "SELECT AcademicYear, ClassID, RoomID FROM RegTClassRoom WHERE PersonID = :PersonID";
					$queryCRNF = $dbh->prepare($sqlCRNF);
					$queryCRNF->execute(array(':PersonID'=>$PersonID));
					$chkCRNF = $queryCRNF->rowCount();
					$resultCRNF = $queryCRNF->fetch(PDO::FETCH_ASSOC);

						$stdYearNow = $resultCRNF['AcademicYear'];
						$stdClassIDNow = $resultCRNF['ClassID'];
						$stdRoomIDNow = $resultCRNF['RoomID'];

						echo  'YearNow = '.$stdYearNow.' : NowClassID = '.$stdClassIDNow.' : NowRoomID = '.$stdRoomIDNow.'<br />';

					if($chkCRNF != 0){
						$yearDiff = 0;
						$yearDiff = ($stdYearNow - $_POST['AcademicYear']);
						$ClassID = ($stdClassIDNow - $yearDiff);
						$RoomID = $stdRoomIDNow;
					}
						echo 'yearDiff = '.$yearDiff.' : OldClassID = '.$ClassID.' : OldRoomID = '.$RoomID.'<br />';
				}


				$sqlTCH = "SELECT TTeacher.NameFirst, TTeacher.NameLast FROM TTeacher, TTeacherActivity WHERE TTeacher.IDTeacher = TTeacherActivity.IDTeacher AND (TTeacherActivity.AYear = ".$_POST['AcademicYear']." AND TTeacherActivity.ActivityCode = '".$importLocal->utf8_to_tis620($SubjectCode)."')";
				//echo $sqlTCH.'<br />';
				$objTCH = odbc_exec($objConnect, $sqlTCH) or die ("Error Execute : [".$sqlTCH."]");
				while($resultTCH = odbc_fetch_array($objTCH)){

					$TeacherFirstName = $importLocal->tis620_to_utf8($resultTCH['NameFirst']);
					$TeacherLastName = $importLocal->tis620_to_utf8($resultTCH['NameLast']);

					echo 'Teacher : '.$TeacherFirstName.' '.$TeacherLastName.'<br />';

					$sqlTID = "SELECT PersonID FROM ComMPerson WHERE PersonTypeID = 1 AND (FirstName = :FirstName AND LastName = :LastName)";
					$queryTID = $dbh->prepare($sqlTID);
					$queryTID->execute(array(':FirstName'=>$TeacherFirstName, ':LastName'=>$TeacherLastName));
					while ($resultTID = $queryTID->fetch(PDO::FETCH_ASSOC)) {

						$TeacherID = $resultTID['PersonID'];
						echo 'Get TeacherID <br />';
					}
					
					echo 'Select Teacher <br />';
				}

					$ClassID = $importLocal->tis620_to_utf8($ClassID);
					$RoomID = $importLocal->tis620_to_utf8($RoomID);

					echo 'SubjectID = '.$SubjectID.' : ClassID = '.$ClassID.' : RoomID = '.$RoomID.' : TeacherID = '.$TeacherID.'<br />';

				$sqlInsert = "INSERT INTO RegTTeach (AcademicYear, TermID, SubjectID, ClassID, RoomID, PersonID)";
				$sqlInsert .= " VALUES (:AcademicYear, :TermID, :SubjectID, :ClassID, :RoomID, :PersonID)";
				$queryInsert = $dbh->prepare($sqlInsert);
				$queryInsert->execute(array(':AcademicYear'=>$_POST['AcademicYear'], ':TermID'=>$TermID,':SubjectID'=>$SubjectID, ':ClassID'=>$ClassID, ':RoomID'=>$RoomID, ':PersonID'=>$TeacherID));
					
				}

				echo '['.$i.'] : PersonID = '.$PersonID.' : SubjectID = '.$SubjectID.' : TermID = '.$TermID.' : GradeID = '.$GradeID.'<br /><br />';

				$i++;
				
		    $sqlCHKTranscript = "SELECT PersonID FROM RegTTranscript WHERE (PersonID = :PersonID AND SubjectID = :SubjectID) AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
			$queryCHKTranscript = $dbh->prepare($sqlCHKTranscript);
			$queryCHKTranscript->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST['AcademicYear'], ':TermID'=>$TermID));
			$CHKTranscript = $queryCHKTranscript->rowCount();

				if($CHKTranscript == 1){

					$sqlTranscript = "UPDATE RegTTranscript SET GradeID = :GradeID WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
					$sqlTranscript .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";

				}else{

					$sqlTranscript = "INSERT INTO RegTTranscript (PersonID, SubjectID, AcademicYear, TermID, GradeID)";
					$sqlTranscript .= " VALUES (:PersonID, :SubjectID, :AcademicYear, :TermID, :GradeID)";
					
				}

					$queryTranscript = $dbh->prepare($sqlTranscript);
					$queryTranscript->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST['AcademicYear'], ':TermID'=>$TermID, ':GradeID'=>$GradeID));
			}


		//echo '2553 = '.$y2553.' : 2554 = '.$y2554.' : 2555 = '.$y2555.' : 2556 = '.$y2556.' : Row = '.($y2553+$y2554+$y2555+$y2556);

	}
?>