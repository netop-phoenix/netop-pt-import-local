<?php
	require_once('config/connect.php');
	require_once('config/function.php');
	require_once('config/importLocal.php');

if($_POST['ImportType'] == 'ActivitySubject'){

	$AcademicYear = $_POST['AcademicYear'];
	$SubjectTypeID = $importLocal->getSubjectTypeID('กิจกรรม');
	$vars = array();

	$strSQL = "SELECT * FROM LT_Activity";
	$objExec = odbc_exec($objConnect, $strSQL) or die ("Error Execute : [".$strSQL."]");
		while($objResult = odbc_fetch_array($objExec))
		{
		    $CurriculumID = $importLocal->getCurriculumID($objResult['PLevel']);
		    $vars[':CurriculumID'] = $importLocal->getCurriculumID($objResult['PLevel']);
		    $vars[':AcademicYear'] = $AcademicYear;
		    $vars[':SubjectCode'] = $importLocal->tis620_to_utf8($objResult['ActivityCode']);
		    $vars[':ReportSubjectCode'] = $importLocal->tis620_to_utf8($objResult['ActivityCode']);
		    $vars[':SubjectName'] = $importLocal->tis620_to_utf8($objResult['ActivityName']);
		    $vars[':Credits'] = $importLocal->tis620_to_utf8($objResult['Weight']);
		    $vars[':SubjectHours'] = $importLocal->tis620_to_utf8($objResult['nHour']);
		    $vars[':SubjectTypeID'] = $SubjectTypeID;
		    $vars[':Version'] = date('YmdHis');

		    $sql = "SELECT SubjectID FROM RegMSubject WHERE (SubjectCode = :SubjectCode AND AcademicYear = :AcademicYear)";
			$stmt = $dbh->prepare($sql);
			$stmt->execute(array(':SubjectCode'=> $vars[':SubjectCode'], ':AcademicYear'=>$AcademicYear));
			$count = $stmt->rowCount();

			if($count == 0){

				$sql = "INSERT INTO RegMSubject (CurriculumID, AcademicYear, SubjectCode, ReportSubjectCode, SubjectName,";
				$sql .= " Credits, SubjectHours, SubjectTypeID, Version) VALUES (:CurriculumID, :AcademicYear, :SubjectCode, :ReportSubjectCode,";
				$sql .= " :SubjectName, :Credits, :SubjectHours, :SubjectTypeID, :Version)";
				$query = $dbh->prepare($sql);
				$query->execute($vars);

			}else{

				$sql = "UPDATE RegMSubject SET CurriculumID = :CurriculumID, SubjectName = :SubjectName, Credits = :Credits,";
				$sql .= " SubjectHours = :SubjectHours, SubjectTypeID = :SubjectTypeID, Version = :Version ";
				$sql .= " WHERE SubjectCode = :SubjectCode AND AcademicYear = :AcademicYear";
				$query = $dbh->prepare($sql);
				$query->execute(array(':CurriculumID'=>$vars[':CurriculumID'], ':SubjectName'=>$vars[':SubjectName'], ':Credits'=>$vars[':Credits'], ':SubjectHours'=>$vars[':SubjectHours'], ':SubjectTypeID'=>$vars[':SubjectTypeID'], ':Version'=>$vars[':Version'], ':SubjectCode'=>$vars[':SubjectCode'], ':AcademicYear'=>$vars[':AcademicYear']));
			}
				
		}
}
?>