<?php

	require_once('config/connect.php');
	require_once('config/function.php');
	require_once('config/importLocal.php');

if($_POST['ImportType'] == 'SL'){

	//$AcademicYear = $_POST['AcademicYear'];

	$strSQL = "SELECT * FROM TStudRWScore WHERE IDYearData LIKE '%".$_POST['AcademicYear']."%'";
	$objExec = odbc_exec($objConnect, $strSQL) or die ("Error Execute : [".$strSQL."]");
	while($objResult = odbc_fetch_array($objExec)) {

				if(!empty($importLocal->tis620_to_utf8($objResult["IDYearData"]))){
					
					$text = $importLocal->tis620_to_utf8($objResult["IDYearData"]);
					$CurriculumNo = $text[0];
					$AcademicYear = substr($text, 1, 4);					
					$TermSName = $text[5];										
					$ClassSName = substr($text, 6, 8);
					$RoomName = substr($text, 9, 10);
					
				}

				if($AcademicYear == $_POST['AcademicYear']){

    				$RW11 = $importLocal->tis620_to_utf8($objResult["RW11"]);
    				$RW12 = $importLocal->tis620_to_utf8($objResult["RW12"]);
    				$RW1Av = $importLocal->tis620_to_utf8($objResult["RW1Av"]);
    				$RW21 = $importLocal->tis620_to_utf8($objResult["RW21"]);
    				$RW22 = $importLocal->tis620_to_utf8($objResult["RW22"]);
    				$RW2Av = $importLocal->tis620_to_utf8($objResult["RW2Av"]);
                    $RWAvg = $importLocal->tis620_to_utf8($objResult["RWAvg"]);
                    $RWResult = $importLocal->tis620_to_utf8($objResult["RWResult"]);

                    $PersonID = $importLocal->getStudentPersonID($importLocal->tis620_to_utf8($objResult["IDStudent"]));
                    $TermID = $importLocal->getTermID($TermSName);
                    $SubjectID = $importLocal->getSubjectID($importLocal->tis620_to_utf8($objResult["PSubjectCode"]),$_POST['AcademicYear']);

				if(empty($RW1Av) && !empty($RW11) && !empty($RW12)){

                    $RW1Av = ($RW11+$RW12)/2;
				}

                if(empty($RW2Av) && !empty($RW21) && !empty($RW22)){

                    $RW2Av = ($RW21+$RW22)/2;
                }

                if(empty($RWAvg) && !empty($RW1Av) && !empty($RW2Av)){

                    $RWAvg = ($RW1Av+$RW2Av)/2;
                }

                if(empty($RWAvg) &&empty($RW21) && empty($RW22) && empty($RW2Av) && !empty($RW1Av)){
                		
                        $RWAvg = $RW1Av;
                }



    				switch ($RWAvg) {
                        case  ($RWAvg == 0):
                            $MODE = 0;
                        break;

                        case  ($RWAvg > 0 AND $RWAvg <= 1):
                            $MODE = 1;
                        break;

                        case  ($RWAvg > 1 AND $RWAvg <= 2):
                            $MODE = 2;
                        break;

                        case  ($RWAvg > 2 AND $RWAvg <= 3):
                            $MODE = 3;
                        break;
                    
                    	default:
                            $MODE = 0;
                        break;
                }

                            $AssessID = $importLocal->getAssessID($MODE);


		                    $sqlCHK = "SELECT PersonID FROM RegTTranscript WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
		                    $sqlCHK .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
						    $stmtCHK = $dbh->prepare($sqlCHK);
						    $stmtCHK->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID));
						    $countCHK = $stmtCHK->rowCount();


                        if($countCHK == 0){

                            $sql = "INSERT INTO RegTTranscript(PersonID, SubjectID, AcademicYear, TermID, AssessLiteratureID, Version)";
                            $sql .= " VALUES(:PersonID, :SubjectID, :AcademicYear, :TermID, :AssessID, :Version)";

                        }else{

                            $sql = "UPDATE RegTTranscript SET AssessLiteratureID = :AssessID, Version = :Version WHERE";
                            $sql .= " (PersonID = :PersonID AND SubjectID = :SubjectID) AND (AcademicYear = :AcademicYear AND TermID = :TermID)";                           
						    

                        }
                        	$stmt = $dbh->prepare($sql);
                            $stmt->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID, ':AssessID'=>$AssessID, ':Version'=>date('YmdHis')));

        			}

   			 }

	}

?>