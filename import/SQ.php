<?php

	require_once('config/connect.php');
	require_once('config/function.php');
	require_once('config/importLocal.php');

if($_POST['ImportType'] == 'SQ'){

	$strSQL = "SELECT * FROM TStudCharScore  WHERE IDYearData LIKE '%".$_POST['AcademicYear']."%'";
	$objExec = odbc_exec($objConnect, $strSQL) or die ("Error Execute : [".$strSQL."]");
	while($objResult = odbc_fetch_array($objExec)) {

		if(!empty($importLocal->tis620_to_utf8($objResult["IDYearData"]))){
					
			$text = $importLocal->tis620_to_utf8($objResult["IDYearData"]);
			$CurriculumNo = $text[0];
			$AcademicYear = substr($text, 1, 4);					
			$TermSName = $text[5];										
			$ClassSName = substr($text, 6, 8);
			$RoomName = substr($text, 9, 10);
					
		}		

			$Total = $objResult["CHTot"];
			$Score1 = $objResult["ChScore1"];
			$Score2 = $objResult["ChScore2"];
			$Total = $objResult["CHTot"];
			$Average = $objResult["ChAvg"];

			$PersonID = $importLocal->getStudentPersonID($importLocal->tis620_to_utf8($objResult["IDStudent"]));
            $TermID = $importLocal->getTermID($TermSName);
            $SubjectID = $importLocal->getSubjectID($importLocal->tis620_to_utf8($objResult["PSubjectCode"]),$_POST['AcademicYear']);


					// echo 'IDYearData : '.$text.'<br />';
					// echo 'CHTot : '.$Total.'<br />';
					// echo 'ChScore1 : '.$Score1.'<br />';
					// echo 'ChScore2 : '.$Score2.'<br />';
					// echo 'CHTot : '.$Total.'<br />';
					// echo 'ChAvg : '.$Average.'<br />';
					// echo '-------------------------------------------<br />';



				if(empty($Total) && !empty($Score1) && !empty($Score2)){

						$Total = $Score1 + $Score2;
						if(empty($Average)){

						$Average = $Total/2; 

						}

				}else if(empty($Total) && !empty($Score1) && empty($Score2)){

						$Total = $Score1;
						if(empty($Average)){
						$Average = $Score1;
						}

				}else if(empty($Total) && empty($Score1) && !empty($Score2)){
						$Total = $Score2;
						if(empty($Average)){
						$Average = $Score2;
						}
				}

				if($Average == 0){
					$Average = 0;
				}else if($Average > 0 && $Average <= 1){
					$Average = 1;
				}else if($Average > 1 && $Average <= 2){
					$Average = 2;
				}else if($Average > 2 && $Average <= 3){
					$Average = 3;
				}


				switch ($objResult["CharNo"]){

					case "1":
						$SQ = "SQ1";
						break;

					case "2":
						$SQ = "SQ2";
						break;

					case "3":
						$SQ = "SQ3";
						break;

					case "4":
						$SQ = "SQ4";
						break;

					case "5":
						$SQ = "SQ5";
						break;

					case "6":
						$SQ = "SQ6";
						break;

					case "7":
						$SQ = "SQ7";
						break;

					case "8":
						$SQ = "SQ8";
						break;

					default:
						$SQ = "";
						break;
				}

				$sqlCHK = "SELECT PersonID FROM RegTTranscript WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
		        $sqlCHK .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
				$stmtCHK = $dbh->prepare($sqlCHK);
				$stmtCHK->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID));
				$countCHK = $stmtCHK->rowCount();


				if(!empty($SQ)){

					if($countCHK == 0){

						$sqlExec = "INSERT INTO RegTTranscript(PersonID, SubjectID, AcademicYear, TermID, ".$SQ.", Version)";
                        $sqlExec .= " VALUES(:PersonID, :SubjectID, :AcademicYear, :TermID, :Average, :Version)";

					}else{

						$sqlExec = "UPDATE RegTTranscript SET ".$SQ." = :Average, Version = :Version WHERE";
                        $sqlExec .= " (PersonID = :PersonID AND SubjectID = :SubjectID) AND (AcademicYear = :AcademicYear AND TermID = :TermID)";

					}

						$stmtExec = $dbh->prepare($sqlExec);
                        $stmtExec->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID, ':Average'=>$Average, ':Version'=>date('YmdHis')));

				}

						$sqlAVG = "SELECT SQ1, SQ2, SQ3, SQ4, SQ5, SQ6, SQ7, SQ8 FROM RegTTranscript";
						$sqlAVG .= " WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
		                $sqlAVG .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
						$stmtAVG = $dbh->prepare($sqlAVG);
						$stmtAVG->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID));
						while ($resultAVG = $stmtAVG->fetch(PDO::FETCH_ASSOC)) {
							$arraySQ = array();
							for ($i=1; $i < 9 ; $i++) { 

								if(!empty($resultAVG["SQ".$i])){

									array_push($arraySQ,$resultAVG["SQ".$i]);
								}
							}

						}

	
					    $resultSQ =  $importLocal->array_mode($arraySQ,0);
					    $AssessID =  $importLocal->getAssessID($resultSQ);

						$sqlSUM = "UPDATE RegTTranscript SET AssessQualityID = :AssessID, Version = :Version WHERE";
                        $sqlSUM .= " (PersonID = :PersonID AND SubjectID = :SubjectID) AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
                        $stmtSUM = $dbh->prepare($sqlSUM);
                        $stmtSUM->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$AcademicYear, ':TermID'=>$TermID, ':AssessID'=>$AssessID, ':Version'=>date('YmdHis')));
	}


}

?>
