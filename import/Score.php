<?php

	// require_once('../config/connect.php');
	// require_once('../config/function.php');
	// require_once('../config/importLocal.php');
	// $importLocal = new importLocal();
	$i = 1;
	$insert = 0;
	$update = 0;

if($_POST['ImportType'] == 'Score'){
	$strSQL = "SELECT * FROM TScore WHERE IDYearData LIKE '%".$_POST['AcademicYear']."%'";
	$objExec = odbc_exec($objConnect, $strSQL) or die ("Error Execute : [".$strSQL."]");
	while($objResult = odbc_fetch_array($objExec)) {

		$IDYearData = $importLocal->IDYearData_Split($objResult["IDYearData"]);
		$PersonID = $importLocal->getStudentPersonID($importLocal->tis620_to_utf8($objResult["IDStudent"]));
        $TermID = $importLocal->getTermID($IDYearData["TermSName"]);
        $SubjectID = $importLocal->getSubjectID($importLocal->tis620_to_utf8($objResult["PSubjectCode"]),$_POST['AcademicYear']);
        $ClassID = $importLocal->getClassID($IDYearData["ClassSName"]);
		$RoomID = $importLocal->getRoomID($IDYearData["RoomName"]);
		$P100Score = $importLocal->tis620_to_utf8($objResult["P100"]);
		$PGrade = $importLocal->tis620_to_utf8($objResult["PGrade"]);
		$PReGrade = $importLocal->tis620_to_utf8($objResult["PReGrade"]);
		$PReGradeScore = $importLocal->tis620_to_utf8($objResult["PReGStudy"]);

		if((empty($PGrade) || is_null($PGrade)) && (empty($P100Score) || is_null($P100Score))){

			$GradeID = 0;
						
		}else if(!empty($PGrade) || !is_null($PGrade)){

			$GradeID = $importLocal->getGradeID($PGrade);

		}else if((empty($PGrade) || is_null($PGrade)) && (!empty($P100Score) || !is_null($P100Score))){

			$GradeID = $importLocal->getGradeIDFromScore($P100Score);						
		}


		if((empty($PReGrade) || is_null($PReGrade)) && (empty($PReGradeScore) || is_null($PReGradeScore))){

			$ReGradeID = 0;
						
		}else if(!empty($PReGrade) || !is_null($PReGrade)){

			$ReGradeID = $importLocal->getGradeID($PReGrade);

		}else if((empty($PReGrade) || is_null($PReGrade)) && (!empty($PReGradeScore) || !is_null($PReGradeScore))){

			$ReGradeID = $importLocal->getGradeIDFromScore($PReGradeScore);						
		}


		if($TermID == '_'){
			$TermID = 0;
		}
		

		if($GradeID == NULL || $GradeID == '' || empty($GradeID)){

			$GradeID = 0;
		}				

			$sqlCHK = "SELECT GradeID FROM RegTTranscript WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
		    $sqlCHK .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
			$stmtCHK = $dbh->prepare($sqlCHK);
			$stmtCHK->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST["AcademicYear"], ':TermID'=>$TermID));
			$countCHK = $stmtCHK->rowCount();
						
			if($countCHK == 0){

				$sqlInsert = "INSERT INTO RegTTranscript(PersonID, SubjectID, AcademicYear, TermID, GradeID, ReGradeID, Version)";
                $sqlInsert .= " VALUES(:PersonID, :SubjectID, :AcademicYear, :TermID, :GradeID, :ReGradeID, :Version)";
                $stmtInsert = $dbh->prepare($sqlInsert);
                $stmtInsert->execute(array(':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST["AcademicYear"], ':TermID'=>$TermID, ':GradeID'=>$GradeID, ':ReGradeID'=>$ReGradeID, ':Version'=>date('YmdHis')));
                $insert++;
			}else{

				$sqlUpdate = "UPDATE RegTTranscript SET GradeID = :GradeID, ReGradeID = :ReGradeID, Version = :Version";
				$sqlUpdate .= " WHERE (PersonID = :PersonID AND SubjectID = :SubjectID)";
				$sqlUpdate .= " AND (AcademicYear = :AcademicYear AND TermID = :TermID)";
				$stmtUpdate = $dbh->prepare($sqlUpdate);
                $stmtUpdate->execute(array(':GradeID'=>$GradeID, ':ReGradeID'=>$ReGradeID, ':Version'=>date('YmdHis'), ':PersonID'=>$PersonID, ':SubjectID'=>$SubjectID, ':AcademicYear'=>$_POST["AcademicYear"], ':TermID'=>$TermID));

				$update++;
			}
						//echo $sqlInsert.'<br />';

                        // echo '['.$i.'] PersonID = '.$PersonID.' : SubjectID = '.$SubjectID.' : AcademicYear = '.$IDYearData["AcademicYear"].' : TermID = '.$TermID.' : GradeID = '.$GradeID.' : ReGradeID = '.$ReGradeID.'<br />';

                        // echo 'Insert = ['.$insert.']  :  Update = ['.$update.']<br />';

                $i++;

	}

}
?>