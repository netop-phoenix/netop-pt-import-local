<?php
	require('config/connect.php');
	require('config/function.php');
	require('config/importLocal.php');

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<title><?php include('config/title.html'); ?></title>
	
<!--[if lt IE 9]>
<script src="js/html5.js" type="text/javascript"></script>
<![endif]-->

<link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/shortcodes.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/animate.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/responsive.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/jplayer.blue.monday.css' type='text/css' media='all' />
<link rel='stylesheet' href='css/flexslider.css' type='text/css' media='all' />

<!-- FontAwesome CSS -->
<link rel="stylesheet" type="text/css" media="all" href="fontawesome/css/font-awesome.css" />

<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.html'></script>
<script type='text/javascript' src='js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='js/hoverIntent.js'></script>
<script type='text/javascript' src='js/superfish.js'></script>
<script type='text/javascript' src='js/jquery.jplayer.min.js'></script>
<script type='text/javascript' src='js/jquery.preloadify.min.js'></script>
<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='js/jquery.fitvids.js'></script>
<script type='text/javascript' src='js/jquery.flexslider.js'></script>
<script type='text/javascript' src='js/waypoints.js'></script>
<script type='text/javascript' src='js/sys_custom.js'></script>
 
<!-- Google Fonts -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css" />
</head>

<body>
<!-- LayoutWrap -->
<div id="stretched" class="leftsidebar">
	<!-- bodyoverlay -->
	<div class="bodyoverlay"></div>
	<!-- Stickybar -->
	<div id="trigger" class="tarrow"></div>
	<div id="sticky">Enter the content which will be displayed in sticky bar </div>

	<!-- #wrapper -->
	<div id="wrapper">
<?php include('config/header.php');
//include('config/subheader.php'); ?>
	
		<div class="pagemid">
			<div class="inner">
				<div id="main">
					<div class="entry-content">

						<div class="post" style="min-height: 475px;">
							
							<div class="two_third">
								<h3 class="fancy-title textleft"  style="line-height:normal;">นำเข้าข้อมูล</h3>
								<div class="wpcf7" id="wpcf7-f455-p28-o1">
								<form action="" method="post" class="wpcf7-form" novalidate="novalidate">
								
								<p>
									<span class="wpcf7-form-control-wrap your-subject">
										<select name="AcademicYear" class="wpcf7-form-control">
											<option value="">เลือกปีการศึกษา</option>
											<?php 
											$year = 2557;
											$yearmin = $year-10;
											$yearmax = $year+3; 
											for ($i=$yearmin; $i < $yearmax; $i++) { 
												echo "<option value='".$i."'>".$i."</option>";
											}
											?>
										</select>
									</span></p>


								<p>
									<span class="wpcf7-form-control-wrap your-subject">
										<select name="ImportType" class="wpcf7-form-control">
											<option value="">เลือกประเภท</option>
											<option value="Register">ข้อมูลการลงทะเบียนวิชา</option>
											<option value="Score">ผลการเรียนนักเรียน</option>
											<option value="Activity">ผลการเรียนวิชากิจกรรม</option>
											<option value="SQ">คะแนนคุณลักษณะอันพึงประสงค์</option>
											<option value="SL">คะแนนอ่านคิดวิเคราะห์เขียน</option>
											<option value="ActivitySubject">ข้อมูลวิชากิจกรรม</option>
										</select>
									</span></p>
								<p style="margin-top:120px;"><input type="submit" value="IMPORT" class="wpcf7-form-control wpcf7-submit" /></p>
								<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
							</div><!-- .two_third-->
							
							<div class="one_third last">
							</div><!-- .one_third-->
							<div class="clear"></div>
						</div><!-- .POST -->
						<div class="clear"></div>
<?php
	if(isset($_POST['ImportType']) && !empty($_POST['ImportType'])){


		$importLocal = new importLocal();
		require('import/Register.php');
		require('import/Score.php');
		require('import/Activity.php');
		require('import/SL.php');
		require('import/SQ.php');
		require('import/ActivitySubject.php');

		//echo "<script>alert('นำเข้าข้อมูล เรียบร้อย !');window.location='index.php';</script>";
	}
?>
					</div><!-- .entry-content -->
				</div><!-- #main -->
				
<?php include('config/sidebar.php'); ?>
				
			</div><!-- .inner -->
		</div><!-- .pagemid -->
		
<?php include('config/footer.php'); ?>
	</div><!-- #wrapper -->
</div>
<?php include('config/back-top.html'); ?>
</body>
</html>